package org.amdatu.examples.rsa.client;

import org.amdatu.examples.rsa.weather.Weather;
import org.amdatu.examples.rsa.weather.WeatherService;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.apache.felix.service.command.CommandProcessor;

@Component(provides=Object.class,
		properties={
		@Property(name=CommandProcessor.COMMAND_SCOPE, value="weather"),
		@Property(name=CommandProcessor.COMMAND_FUNCTION, value={"getWeather"})
})
public class Commands {
	
	@ServiceDependency
	private volatile WeatherService weatherService;
	
	
	public void getWeather(String city) {
		Weather weatherInfo = weatherService.getWeatherInfo(city);
		
		System.out.println(weatherInfo);
	}
}
