package org.amdatu.examples.rsa.weather.impl;

import java.io.IOException;
import java.net.URL;

import org.amdatu.examples.rsa.weather.Weather;
import org.amdatu.examples.rsa.weather.WeatherService;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;
import org.osgi.service.remoteserviceadmin.RemoteConstants;

import com.fasterxml.jackson.databind.ObjectMapper;

@Component(properties=
	@Property(name=RemoteConstants.SERVICE_EXPORTED_INTERFACES, 
				value="org.amdatu.examples.rsa.weather.WeatherService"))
public class OpenWeatherMapService implements WeatherService{

	@Override
	public Weather getWeatherInfo(String city) {
		ObjectMapper mapper = new ObjectMapper();

		try {
			Weather weather = mapper.readValue(new URL("http://api.openweathermap.org/data/2.5/weather?q="+ city), Weather.class);
			return weather;
		} catch (IOException e) {
			e.printStackTrace();
			
			throw new RuntimeException("Error reading weather info", e);
		}

	}
}
