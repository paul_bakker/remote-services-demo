package org.amdatu.examples.rsa.weather;


public interface WeatherService {

	Weather getWeatherInfo(String city);

}
