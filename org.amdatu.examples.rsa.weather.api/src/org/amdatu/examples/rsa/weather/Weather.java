package org.amdatu.examples.rsa.weather;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Weather {
	@JsonProperty("weather")
	private List<WeatherDescription> descriptions;

	@JsonProperty("main")
	private BasicWeatherData basicWeatherData;

	public List<WeatherDescription> getDescriptions() {
		return descriptions;
	}

	public void setDescriptions(List<WeatherDescription> descriptions) {
		this.descriptions = descriptions;
	}

	public BasicWeatherData getBasicWeatherData() {
		return basicWeatherData;
	}

	public void setBasicWeatherData(BasicWeatherData basicWeatherData) {
		this.basicWeatherData = basicWeatherData;
	}

	@Override
	public String toString() {
		return "Weather [descriptions=" + descriptions + ", basicWeatherData="
				+ basicWeatherData + "]";
	}

	
}
